package com.joni;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testHashSet(){
		
		HashSet<properties> restProperties = new HashSet<>();
		HashSet<properties> dbProperties = new HashSet<>();
		
		restProperties.add(new properties ("cellermobile", "123"));
		restProperties.add(new properties ("mobile", "123"));
		restProperties.add(new properties ("cellmobile", "123"));
		dbProperties.add(new properties ("cellmobile", "456"));
		dbProperties.add(new properties ("mobile", "456"));
		
		HashSet<properties> CopyOfDbProperties = dbProperties;
		
		Iterator<properties> iter = restProperties.iterator();
		List<String> keys = new ArrayList<>();
		
		while(iter.hasNext()){
			keys.add(iter.next().key);
		}
			
		
		//printResults(CopyOfDbProperties);
	}

	private void printResults(HashSet<properties> dbProperties) {
		for(properties props : dbProperties) {
			System.out.println(props.getKey()+ " - "
					+props.getValue());
		}
	}

	public class properties{
		
		public properties(String key, String value) {
			this.key = key;
			this.value = value;
		}
		private String key;
		private String value;
		
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		
	}
}
