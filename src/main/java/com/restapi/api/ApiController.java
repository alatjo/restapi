package com.restapi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.restapi.dto.User;
import com.restapi.repository.UserRepository;

@Controller
@RequestMapping("/api")
@CrossOrigin("http://localhost:3000")
public class ApiController {
	
	@Autowired
	UserRepository userRepository;

	
    @RequestMapping(path="/users",method=RequestMethod.GET)
    public @ResponseBody List<User> getUsers() {
    	List<User> userList = userRepository.findAll();
    	userList.sort((u1, u2) -> u1.username.compareToIgnoreCase(u2.username));
    	return userList;
    }
    
    @RequestMapping(value="/userbyusername/{username}",method=RequestMethod.GET)
    public @ResponseBody User getUserByName(@PathVariable ("username") String username) {
        return userRepository.findUserByUsername(username);
    }
    
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public @ResponseBody boolean login() {
		return true;
    }
}
