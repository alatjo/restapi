package com.restapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.restapi.dto.User;

public interface UserRepository extends MongoRepository<User, String> {
	
	@Query("{ 'username' : ?0 }")
	public User findUserByUsername(String username);
}
