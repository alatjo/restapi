package com.restapi.utils;

import java.net.Inet4Address;
import java.net.UnknownHostException;

public abstract class Utils {
	public static String getIpAddress(){
		try {
			return Inet4Address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return "Something went wrong";
		}
	}
}
