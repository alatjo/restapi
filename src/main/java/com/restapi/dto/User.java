package com.restapi.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.restapi.utils.Utils;

@Document(collection="users")
public class User {
	
	@Id
	public String id;
	@NotNull
	@Min(5)
	public String username;
	@NotNull
	public String company;
	@NotNull
	public String email;
	public enum gender {
		MALE, FEMALE
	}
	public String address;
	public String postalCode;
	public String city;
	
	public String ip;
	
	//this is for demo purposes for seeing container/node IP-address in response
	public String getIp(){
		return Utils.getIpAddress();
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	};
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getId() {
		return id;
	}

	
}
