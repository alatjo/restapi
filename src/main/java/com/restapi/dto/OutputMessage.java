package com.restapi.dto;

public class OutputMessage {

    private String key;
    private String from;
    private String text;
    private String time;
    private boolean isRead;

    public OutputMessage(final String key, final String from, final String text, final boolean isRead, final String time) {
        this.key = key;
    	this.from = from;
        this.text = text;
        this.time = time;
        this.isRead = isRead;
    }
    
    public String getKey() {
        return key;
    } 

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

    public String getFrom() {
        return from;
    }

	public boolean isRead() {
		return isRead;
	}
}
