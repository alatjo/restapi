package com.restapi.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
	
	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
	    auth
	        .inMemoryAuthentication()
	            .withUser("user").password("pw").roles("USER", "ADMIN").and()
	            .withUser("rest").password("pw").roles("USER");
	}
	
    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfig extends WebSecurityConfigurerAdapter{
    	
    	@Autowired AuthenticationEntryPoint authEntryPoint;
    	
    	protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.antMatcher("/api/**")
					.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll()
					.and()
					.authorizeRequests().antMatchers(HttpMethod.GET, "/api/**").hasAnyRole("USER")
					.and()
					.authorizeRequests().antMatchers(HttpMethod.POST, "/api/**").hasAnyRole("ADMIN")
				.and()
				.httpBasic().authenticationEntryPoint(authEntryPoint);
		}
    }

    @Configuration
    public static class FormWebSecurityConfig extends WebSecurityConfigurerAdapter{

        @Override
        protected void configure(HttpSecurity http) throws Exception {
        	http.csrf().disable()
				.authorizeRequests().anyRequest().hasAnyRole("ADMIN")
			.and()
			.formLogin().loginPage("/login").permitAll()
			.and()
			.logout().permitAll().clearAuthentication(true);
        }
    }

}
