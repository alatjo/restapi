package com.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.restapi.dto.User;
import com.restapi.repository.UserRepository;
import com.restapi.utils.Utils;

@Controller
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping("/index")
	public String index(Model model){
		model.addAttribute("ip", Utils.getIpAddress());
		model.addAttribute("userList", userRepository.findAll());		
		return "index";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(@ModelAttribute User user){
		userRepository.save(user);		
		userRepository.findAll();
		return "redirect:index";
	}
	
	@RequestMapping(value="/removeUser/{id}",method=RequestMethod.GET)
	public String removeUser(@PathVariable ("id") String id){
		User user = userRepository.findOne(id);
		if(user != null) {
			userRepository.delete(user);
			userRepository.findAll();
			//TODO proper logging
			System.out.println("delete user by id: "+id);
		}
		return "redirect:/index";
	}
	
}
