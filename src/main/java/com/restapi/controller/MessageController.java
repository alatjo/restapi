package com.restapi.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.restapi.dto.Message;
import com.restapi.dto.OutputMessage;

@Controller
public class MessageController {

	@MessageMapping("/chat")
	@SendTo("/topic/messages")
	public OutputMessage send(Message message) throws Exception {
	    String time = new SimpleDateFormat("HH:mm").format(new Date());
	    boolean isRead = false;
	    return new OutputMessage(String.valueOf(new Date().getTime()), message.getFrom(), message.getText(), isRead, time);
	}
}
