# restApi README

## Prerequisites:  
> 1.Java 8 installed  
> 2.Maven installed  
> 3.MongoDB instance running on localhost 27017 port  
> 4.Redis instance running on localhost 6379 port
>
> OR
>
> if using docker-compose option then Docker and Docker-Compose should be installed

##What is this?  
> Spring boot application which provides restApi service.  
> Application uses as a backend provider MongoDB.  
> Application has login functionality and session info will be stored in Redis DB.  

##How to use?  
>App URL:  
>>http://localhost/index  
>
>App Login info:  
> > username: user  
> > password: pw  
>
>RestApi:  
>> http://localhost/api/users (all users)  
>> http://localhost/api/userbyusername/<username>/ (get user by username)
>
>Build and Run (whole development infra):
>
> Docker Compose
>
>>		docker-compose up -d --build //build & start
>>		docker-compose down //stop & destroy
>
>(or) Build and Run:
>
> Build:
> 
>>     mvn clean install  
>     #or
>     mvn clean package
>
> Run:
> 
>>     java -jar demo-0.0.1-SNAPSHOT.jar

##Troubleshooting  
>Redis or mongoDB connection does not work?
> Check container ip-addresses via docker command:
> 
>>     docker inspect restapicluster_mongo_1 | grep IPAddress
>>     docker inspect restapicluster_redis_1 | grep IPAddress
>Update these addresses to the /restApi/application-dev.properties and re-compose your infra